package billsPayment

import java.time.LocalDate

interface billsPaymentInterface {
    var accountNumber: Int
    var accountName: String
    var amountDue: Double
}